package game

import java.awt.event.KeyEvent
import java.awt.event.KeyListener

class Keyboard extends KeyListener {
  def keyPressed(e: KeyEvent) {
    if (Main.scene.mario.isAlive == true) {
      if (e.getKeyCode == KeyEvent.VK_RIGHT) {
        // per non fare muovere il castello e start
        if (Main.scene.getxPos == -1) {
          Main.scene.setxPos(0)
          Main.scene.setBackground1PosX(-50)
          Main.scene.setBackground2PosX(750)
        }
        Main.scene.mario.setMoving(true)
        Main.scene.mario.setToRight(true)
        Main.scene.setMov(1) // si muove verso sinistra
      }
      else if (e.getKeyCode == KeyEvent.VK_LEFT) {
        if (Main.scene.getxPos == 4601) {
          Main.scene.setxPos(4600)
          Main.scene.setBackground1PosX(-50)
          Main.scene.setBackground2PosX(750)
        }
        Main.scene.mario.setMoving(true)
        Main.scene.mario.setToRight(false)
        Main.scene.setMov(-1) // si muove verso destra
      }
      // salto
      if (e.getKeyCode == KeyEvent.VK_UP) {
        Main.scene.mario.setJumping(true)
        Audio.playSound("/resources/audio/jump.wav")
      }
    }
  }

  def keyReleased(e: KeyEvent) {
    Main.scene.mario.setMoving(false)
    Main.scene.setMov(0)
  }

  def keyTyped(e: KeyEvent) {
  }
}