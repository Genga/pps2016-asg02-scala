package game

import javax.sound.sampled.AudioInputStream
import javax.sound.sampled.AudioSystem
import javax.sound.sampled.Clip

object Audio {
  def playSound(son: String) {
    val s: Audio = new Audio(son)
    s.play()
  }
}

class Audio(val son: String) {

  private var clip: Clip = null

  try {
    val audio: AudioInputStream = AudioSystem.getAudioInputStream(getClass.getResource(son))
    clip = AudioSystem.getClip
    clip.open(audio)
  }
  catch {
    case e: Exception => {
    }
  }

  def getClip: Clip = {
    return clip
  }

  def play() {
    clip.start()
  }

  def stop() {
    clip.stop()
  }
}