package characters;

import java.awt.Image;

import utils.Res;
import utils.Utils;
import objects.GameObject;

public class Mushroom extends BasicCharacter implements Runnable {

    public static final int WIDTH = 27;
    public static final int HEIGHT = 30;

    private Image img;
    private final int PAUSE = 15;
    private int offsetX;

    public Mushroom(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.setToRight(true);
        this.setMoving(true);
        this.offsetX = 1;
        this.img = Utils.getImage(Res.IMG_MUSHROOM_DEFAULT);

        Thread chronoMushroom = new Thread(this);
        chronoMushroom.start();
    }

    //getters
    public Image getImg() {
        return img;
    }

    public void move() {
        this.offsetX = isToRight() ? 1 : -1;
        this.setX(this.getX() + this.offsetX);

    }

    @Override
    public void run() {
        while (true) {
            if (this.alive == true) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public void contact(GameObject obj) {
        if (this.hitAhead(obj) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(obj) && !this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }
    }

    public void contact(BasicCharacter pers) {
        if (this.hitAhead(pers) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(pers) && !this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }
    }

    public Image deadImage() {
        return Utils.getImage(this.isToRight() ? Res.IMG_MUSHROOM_DEAD_DX : Res.IMG_MUSHROOM_DEAD_SX);
    }
}
