package characters;

import java.awt.Image;

import utils.Res;
import utils.Utils;
import objects.GameObject;


public class Turtle extends BasicCharacter implements Runnable {

    public static final int WIDTH = 43;
    public static final int HEIGHT = 50;

    private Image imgTurtle;
    private final int PAUSE = 15;
    private int dxTurtle;

    public Turtle(int X, int Y) {
        super(X, Y, WIDTH, HEIGHT);
        super.setToRight(true);
        super.setMoving(true);
        this.dxTurtle = 1;
        this.imgTurtle = Utils.getImage(Res.IMG_TURTLE_IDLE);

        Thread chronoTurtle = new Thread(this);
        chronoTurtle.start();
    }

    public Image getImgTurtle() {
        return imgTurtle;
    }

    public void move() {
        this.dxTurtle = isToRight() ? 1 : -1;
        super.setX(super.getX() + this.dxTurtle);
    }

    @Override
    public void run() {
        while (true) {
            if (this.alive) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public void contact(GameObject obj) {
        if (this.hitAhead(obj) && this.isToRight()) {
            this.setToRight(false);
            this.dxTurtle = -1;
        } else if (this.hitBack(obj) && !this.isToRight()) {
            this.setToRight(true);
            this.dxTurtle = 1;
        }
    }

    public void contact(BasicCharacter pers) {
        if (this.hitAhead(pers) == true && this.isToRight() == true) {
            this.setToRight(false);
            this.dxTurtle = -1;
        } else if (this.hitBack(pers) == true && this.isToRight() == false) {
            this.setToRight(true);
            this.dxTurtle = 1;
        }
    }

    public Image deadImage() {
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }
}
