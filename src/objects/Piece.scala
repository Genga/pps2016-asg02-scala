package objects

import utils.Res
import utils.Utils
import java.awt.Image

object Piece {
  val WIDTH: Int = 30
  val HEIGHT: Int = 30
  val FLIP_FREQUENCY: Int = 100
}

class Piece(x: Int, y: Int) extends GameObject(x, y, Piece.WIDTH, Piece.HEIGHT) with Runnable {
  imgObj = Utils.getImage(Res.IMG_PIECE1)
  private var counter: Int = 0

  def imageOnMovement: Image = {
    return Utils.getImage(if ( {
      this.counter += 1; this.counter
    } % Piece.FLIP_FREQUENCY == 0) Res.IMG_PIECE1
    else Res.IMG_PIECE2)
  }

  def run() {
    while (true) {
      {
        this.imageOnMovement
        try {
          Thread.sleep(10)
        }
        catch {
          case e: InterruptedException => {
          }
        }
      }
    }
  }
}