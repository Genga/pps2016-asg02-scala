package objects

import utils.Res
import utils.Utils

object Tunnel {
  val WIDTH: Int = 43
  val HEIGHT: Int = 65
}

class Tunnel(x: Int, y: Int) extends GameObject(x, y, Tunnel.WIDTH, Tunnel.HEIGHT) {
  imgObj = Utils.getImage(Res.IMG_TUNNEL)
}