package objects

import java.awt.Image
import game.Main

class GameObject(var x: Int, var y: Int, var width: Int, var height: Int) {
  protected var imgObj: Image = null

  def getWidth: Int = {
    return width
  }

  def getHeight: Int = {
    return height
  }

  def getX: Int = {
    return x
  }

  def getY: Int = {
    return y
  }

  def getImgObj: Image = {
    return imgObj
  }

  def setWidth(width: Int) {
    this.width = width
  }

  def setHeight(height: Int) {
    this.height = height
  }

  def setX(x: Int) {
    this.x = x
  }

  def setY(y: Int) {
    this.y = y
  }

  def move() {
    if (Main.scene.getxPos >= 0) {
      this.x = this.x - Main.scene.getMov
    }
  }
}