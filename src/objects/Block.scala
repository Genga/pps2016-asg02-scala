package objects

import utils.Res
import utils.Utils

object Block {
  val WIDTH: Int = 30
  val HEIGHT: Int = 30
}

class Block(x: Int, y: Int) extends GameObject(x, y, Block.WIDTH, Block.HEIGHT) {
  imgObj = Utils.getImage(Res.IMG_BLOCK)
}